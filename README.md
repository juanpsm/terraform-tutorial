# Terraform Tutorial

To deploy infrastructure with Terraform:

* Scope - Identify the infrastructure for your project.
* Author - Write the configuration for your infrastructure.
* Initialize - Install the plugins Terraform needs to manage the infrastructure.
* Plan - Preview the changes Terraform will make to match your configuration.
* Apply - Make the planned changes.

## Install Terraform

Im not following this part of the tutorial, because i want to be
able to manage different Terraform versions with `tfswitch`.

Details in [`instalar_terraform.md`](./instalar_terraform.md)
(in spanish)

## Quickstart

Provision an NGINX server in less than a minute using [Docker](https://docs.docker.com/engine/install/).

```bash
mkdir learn-terraform-docker-container
cd learn-terraform-docker-container
nano main.tf
```

```terraform
terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}

provider "docker" {}

resource "docker_image" "nginx" {
  name         = "nginx:latest"
  keep_locally = false
}

resource "docker_container" "nginx" {
  image = docker_image.nginx.latest
  name  = "tutorial"
  ports {
    internal = 80
    external = 8000
  }
}
```

```bash
terraform init   # downloads a plugin that allows Terraform to interact with Docker
terraform apply  # provision the NGINX server container. Type yes
```

Check [`localhost:8000`](http://localhost:8000/]

```bash
$ docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED          STATUS          PORTS                  NAMES
a9bfd8200a45   62d49f9bab67   "/docker-entrypoint.…"   14 minutes ago   Up 13 minutes   0.0.0.0:8000->80/tcp   tutorial

$ terraform destroy  # stop the container
```

## Build Infrastructure

### Prerequisites

To follow this tutorial you will need:

* [x] The Terraform CLI (0.14.9+) installed.
* [ ] The [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html) installed.
  
  ```bash
  $ curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
  $ unzip awscliv2.zip
  $ sudo ./aws/install
  $ aws --version
  aws-cli/2.2.16 Python/3.8.8 Linux/5.8.0-59-generic exe/x86_64.ubuntu.20 prompt/off
  ```

* [x] An AWS account.
* [x] AWS credentials. (in ticket #5560)

### Write configuration

```bash
$ aws configure
AWS Access Key ID [None]: AKI****************A
AWS Secret Access Key [None]: r1x*******/******************/********g
Default region name [None]: sa-east-1
Default output format [None]: 
```

```bash
mkdir learn-terraform-aws-instance && cd learn-terraform-aws-instance
nano main.tf
```

```terraform
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-west-2"
}

resource "aws_instance" "app_server" {
  ami           = "ami-830c94e3"
  instance_type = "t2.micro"

  tags = {
    Name = "ExampleAppServerInstance"
  }
}
```

* `terraform{}` block: contains Terraform settings, including the required
  providers to provision the infrastructure.
  
  `required_providers` block: constrain provider version. If not specified it
  downloads the most recent version during initialization. Read more in the
  [provider source documentation](https://www.terraform.io/docs/language/providers/requirements.html).

* `provider` block: configure provider, `aws`. A provider is a plugin to create
  and manage resources.

  `profile` attribute: refers Terraform to the AWS credentials stored in the
  AWS configuration file (`~/.aws/credentials`). **Never hard-code credentials
  or other secrets in your Terraform configuration files**.

  multiple provider blocks: e.g. pass the IP address of your AWS EC2 instance to
  a monitoring resource from DataDog.

* `resource` blocks: define components of the infrastructure. Physical or
  virtual components, e.g. EC2 instance, or logical , as a Heroku application.
  
  Strings before the block: unique ID for the resource (`aws_instance.app_server`)
  
  * resource type: `aws_instance`. prefix maps to the name of the provider.
  
  * resource name: `app_server`.

  Resource blocks arguments: machine sizes, disk image names, or VPC IDs. More
  in the [providers reference](https://www.terraform.io/docs/providers/index.html) documents.

### Initialize the directory

```bash
$ terraform init # installs aws provider in .terraform/providers/registry.terraform.io/hashicorp/aws/3.48.0/linux_amd64
$ cat .terraform.lock.hcl  # specifies the exact provider versions used
# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "3.48.0"
  constraints = "~> 3.27"
  hashes = [
...
```

### Format and validate the configuration

```bash
$ terraform fmt  # Format configuration. Prints out modified files names, if any.
$ terraform validate
Success! The configuration is valid.
```

### Create infrastructure

```bash
$ terraform apply

...

    Enter a value: yes
```

Prints out the *execution plan* which describes the actions Terraform will take
in order to change your infrastructure to match the configuration.

Similar to the diff in Git: **`+`** means TF will create this resource.
`(known after apply)` means that the value will not be known until the resource
is created.

Visit the [EC2 console](https://console.aws.amazon.com/ec2/v2/home?region=us-west-2#Instances:sort=instanceId)

### Inspect state

When you applied your configuration, TF wrote data into a file called `terraform.tfstate`.
It stores the IDs and properties of the resources for updating or destroying later.

That is the **only way** TF can track which resources it manages, and often contains **sensitive information**, so you must store your state file securely and restrict access.
Production: [storing state remotely](https://learn.hashicorp.com/tutorials/terraform/cloud-migrate?in=terraform/cloud) with Terraform Cloud, Terraform Enterprise or other [remote backends](https://www.terraform.io/docs/language/settings/backends/index.html).

Inspect the current state:

```bash
$ terraform show
```

<details>
  <summary>
    Show State
  </summary>
  
  ```bash
  # aws_instance.app_server:
  resource "aws_instance" "app_server" {
      ami                                  = "ami-830c94e3"
      arn                                  = "arn:aws:ec2:us-west-2:333591686509:instance/i-0455db5e51eb2d20e"
      associate_public_ip_address          = true
      availability_zone                    = "us-west-2b"
      cpu_core_count                       = 1
      cpu_threads_per_core                 = 1
      disable_api_termination              = false
      ebs_optimized                        = false
      get_password_data                    = false
      hibernation                          = false
      id                                   = "i-0455db5e51eb2d20e"
      instance_initiated_shutdown_behavior = "stop"
      instance_state                       = "running"
      instance_type                        = "t2.micro"
      ipv6_address_count                   = 0
      ipv6_addresses                       = []
      monitoring                           = false
      primary_network_interface_id         = "eni-0505ff8c00c954ab8"
      private_dns                          = "ip-172-31-31-69.us-west-2.compute.internal"
      private_ip                           = "172.31.31.69"
      public_dns                           = "ec2-18-237-72-234.us-west-2.compute.amazonaws.com"
      public_ip                            = "18.237.72.234"
      secondary_private_ips                = []
      security_groups                      = [
          "default",
      ]
      source_dest_check                    = true
      subnet_id                            = "subnet-da48e7a2"
      tags                                 = {
          "Name" = "ExampleAppServerInstance"
      }
      tags_all                             = {
          "Name" = "ExampleAppServerInstance"
      }
      tenancy                              = "default"
      vpc_security_group_ids               = [
          "sg-3baecd1e",
      ]

      capacity_reservation_specification {
          capacity_reservation_preference = "open"
      }

      credit_specification {
          cpu_credits = "standard"
      }

      enclave_options {
          enabled = false
      }

      metadata_options {
          http_endpoint               = "enabled"
          http_put_response_hop_limit = 1
          http_tokens                 = "optional"
      }

      root_block_device {
          delete_on_termination = true
          device_name           = "/dev/sda1"
          encrypted             = false
          iops                  = 0
          tags                  = {}
          throughput            = 0
          volume_id             = "vol-052f7511c7a31c1a1"
          volume_size           = 8
          volume_type           = "standard"
      }
  }
  ```
  
</details><br>

#### Manually Managing State

List of resources en project's state:

```bash
$ terraform state list
aws_instance.app_server
```

## Change Infrastructure

To update the ami of the instance change the `aws_instance.app_server` resource under the provider block in `main.tf` by replacing the current AMI ID with a new one.
> Tip: The snippet is formatted as a diff. Replace the content displayed in red with the content displayed in green, leaving out the leading + and - signs.

```diff
 resource "aws_instance" "app_server" {
-  ami           = "ami-830c94e3"
+  ami           = "ami-08d70e59c07c61a3a"
   instance_type = "t2.micro"
 }
```

> Note: The new AMI ID used in this configuration is specific to the `us-west-2` region. Select an appropriate AMI for that region by following [these instructions](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/finding-an-ami.html#finding-quick-start-ami).

This update changes the AMI to an Ubuntu 16.04 AMI. The AWS provider knows that it **cannot change the AMI of an instance after it has been created**, so Terraform will destroy the old instance and create a new one.

### Apply Changes

Run terraform apply again to see how Terraform will apply this change to the existing resources.

```bash
$ terraform apply
aws_instance.app_server: Refreshing state... [id=i-0455db5e51eb2d20e]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the
following symbols:
-/+ destroy and then create replacement

Terraform will perform the following actions:

  # aws_instance.app_server must be replaced
-/+ resource "aws_instance" "app_server" {
      ~ ami                                  = "ami-830c94e3" -> "ami-08d70e59c07c61a3a" # forces replacement
      ~ arn                                  = "arn:aws:ec2:us-west-2:333591686509:instance/i-0455db5e51eb2d20e" -> (known after apply)
...

  Enter a value: yes

...

Apply complete! Resources: 1 added, 0 changed, 1 destroyed.

```

* The prefix **`-/+`** means TF will **destroy and recreate** the resource, rather than updating it in-place. Changing the AMI for an EC2 instance cannot be done this way, it requires recreating it
* Some attributes can be **updated in-place** (indicated with the **`~`** prefix).
* The execution plan shows that the AMI change is what forces Terraform to replace the instance (comment `# forces replacement`).
* Adjust changes to to avoid destructive updates if necessary.
* Use `terraform show` again to have Terraform print out the new values associated with this instance.

## Destroy Infrastructure

The `terraform destroy` is the inverse of `terraform apply`, it terminates all the resources specified in the Terraform state. It does not destroy resources running elsewhere (not managed by the current Terraform project).

Destroy the resources you created.

```bash
$ terraform destroy 
aws_instance.app_server: Refreshing state... [id=i-0c9f541aa91d9f6b8]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the
following symbols:
  - destroy

Terraform will perform the following actions:

  # aws_instance.app_server will be destroyed
  - resource "aws_instance" "app_server" {
      - ami                                  = "ami-08d70e59c07c61a3a" -> null

...

  Enter a value: yes

Destroy complete! Resources: 1 destroyed.
```

In this case, Terraform identified a **single instance** with no other dependencies, so it destroyed the instance. In more complicated cases with multiple resources, Terraform will destroy them in a suitable order to respect **dependencies**.

## Define Input Variables

Examples so far have used hard-coded values. Terraform configurations can include variables to make your configuration more dynamic and flexible.

To add a variable to define the instance name, create `variables.tf` with a block defining a new `instance_name` variable:

```terraform
variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "ExampleAppServerInstance"
}
```

> Note: Terraform loads all files in the current directory ending in `.tf`, so you can name your configuration files however you choose.

In `main.tf`, update the `aws_instance` resource block to use the new variable:

```diff
 resource "aws_instance" "app_server" {
   ami           = "ami-08d70e59c07c61a3a"
   instance_type = "t2.micro"

   tags = {
-    Name = "ExampleAppServerInstance"
+    Name = var.instance_name
   }
 }
```

When using `terraform apply` the variable will default to its default value ("ExampleAppServerInstance"). To override the default instance name use
the `-var` flag:

```bash
terraform apply -var "instance_name=BeautifullShiningInstance"

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the
following symbols:
  + create

Terraform will perform the following actions:

  # aws_instance.app_server will be created
  + resource "aws_instance" "app_server" {
      + ami                                  = "ami-08d70e59c07c61a3a"
...
      + instance_type                        = "t2.micro"
...
      + tags                                 = {
          + "Name" = "BeautifullShiningInstance"
        }
...
```

Read more: [Customize Terraform Configuration with Variables](https://learn.hashicorp.com/tutorials/terraform/variables?in=terraform/configuration-language).

## Query Data with Outputs

New file `outputs.t`:

```terraform
output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.app_server.id
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.app_server.public_ip
}
```

Now when using `terraform apply` it prints output values to the screen.
Also using the `terraform output` command:

```bash
$ terraform output
instance_id = "i-025918dd50d6d470d"
instance_public_ip = "52.27.54.85"
```

Read more: [Output Data from Terraform](https://learn.hashicorp.com/tutorials/terraform/outputs?in=terraform/configuration-language).

## [Store Remote State](https://learn.hashicorp.com/tutorials/terraform/aws-remote?in=terraform/aws-get-started)

For now this is not necesary.

## SSH access

* [aws-vault](https://github.com/99designs/aws-vault#readme)
* Create [key pair](https://us-west-2.console.aws.amazon.com/ec2/v2/home?region=us-west-2#KeyPairs:)
  * O from console:
  
  ```bash
  aws ec2 create-key-pair --key-name my-key-pair --query "KeyMaterial" --output text > my-key-pair.pem
  ```

  * Save in  `~/.ssh`
  * chmod 400 my-key-pair.pem

More info: [Get info about instaces](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/connection-prereqs.html#connection-prereqs-get-info-about-instance)

> For Amazon Linux 2 or the Amazon Linux AMI, the user name is `ec2-user`.

In this example no it's ubuntu

To connect via ssh:

* Create / add ip to [security group](https://us-west-2.console.aws.amazon.com/ec2/v2/home?region=us-west-2#SecurityGroups:)

* ```bash
  # ssh -i /path/my-key-pair.pem my-instance-user-name@my-instance-public-dns-name
  # ssh -i /path/my-key-pair.pem my-instance-user-name@my-instance-IPv6-address
  ssh -i ~/.ssh/juanpablo.pem ubuntu@ec2-54-218-66-201.us-west-2.compute.amazonaws.com
  ```

Unknown error:

```bash
load pubkey /home/.../juanpablo.pem : invalid format
```

## Provision

### [Source](https://dev.to/mariehposa/how-to-deploy-an-application-to-aws-ec2-instance-using-terraform-and-ansible-3e78)

Virtual enviroment:

```bash
pipenv install ansible
pipenv shell
```

For now the we make a manual configuration.

Hosts file:

```bash
mkdir ansible
nano ansible/hosts
```

```ansible
[frontend]
# <ipv4-public-dns> ansible_ssh_private_key_file=/path/to/key.pem
ec2-34-212-110-250.us-west-2.compute.amazonaws.com ansible_ssh_private_key_file=~/.ssh/juanpablo.pem
```

```bash
nano ansible.cfg
```

```ansible
[defaults]
host_key_checking = False
private_key_file=~/.ssh/juanpablo.pem
inventory=ansible/hosts
remote_user = ubuntu

[ssh_connection]
# control_path=%(directory)s/%%h-%%r
# control_path_dir=~/.ansible/cp
#pipelining = True
scp_if_ssh = True
```

Test hosts:

```bash
$ ansible all --list-hosts
  hosts (1):
    ec2-34-212-110-250.us-west-2.compute.amazonaws.com
```

Finally test theconection:

```bash
$ ansible all -m ping
ec2-34-212-110-250.us-west-2.compute.amazonaws.com | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
```

## Generate ansible inventory

Once created the instance, terraform can create files following templates.

First save variable values usinf function [templatefile](https://www.terraform.io/docs/language/functions/templatefile.html)
in file `outputs.tf`

```terraform
...
resource "local_file" "ansible_hosts" {
  content  = templatefile("${path.module}/templates/hosts.tpl", {
     vm_public_dns = aws_instance.app_server.public_dns,
     vm_key_file   = var.ssh_key_file
  })
  filename = "ansible/hosts"
}
```

Then using the [template syntax](https://www.terraform.io/docs/language/expressions/strings.html#string-templates), file `templates/hosts.tpl`

```tpl
[frontend]

${vm_public_dns} ansible_ssh_private_key_file=${vm_key_file}
```

The same is done for `ansible.cfg`

## Actually provisioning the instance

Once the instance is running and the ansible configuration
generated, playbooks can be run by:

```bash
ansible-playbook playbook-test.yml
```

> Note: asuming working directory is the same where `ansible.cfg`
> and `playbook-test.yml` are located.

A way to automate this at creation time would be using a
[local-exec Provisioner](https://www.terraform.io/docs/language/resources/provisioners/local-exec.html) in the `aws_instance` resource:

```terraform
resource "aws_instance" "app_server" {
  ...
  provisioner "local-exec" {
    command = "ansible-playbook /path/to/playbook-test.yml
  }
}
```

This does not work as intended because the instace can not be ready
at the time that command is executed.

In this tutorial [Deploying to AWS with Terraform and Ansible Repo](https://github.com/linuxacademy/content-deploying-to-aws-ansible-terraform/blob/33ed7b6731696af82a89d51d09f043183416464f/aws_la_cloudplayground_multiple_workers_version/instances.tf#L38)
they use a workaround:

```terraform
resource "aws_instance" "app_server" {
  ...
  provisioner "local-exec" {
    command = <<EOF
aws --profile ${var.profile} ec2 wait instance-status-ok --region ${var.region-master} --instance-ids ${self.id} \
&& ansible-playbook --extra-vars 'passed_in_hosts=tag_Name_${self.tags.Name}' ansible_templates/install_jenkins.yaml
EOF
  }
}
```

Trying to do this:

```bash
$ aws --profile juanpablo ec2 wait instance-status-ok --region us-west-2 --instance-ids i-07bab8d5e37253da0

Unable to locate credentials. You can configure credentials by running "aws configure".
```

Thats because i ran `aws configure` not `aws configure --profile juanpablo` so, im using
the default profile.

After creating a variable for the region, this can be implemented:

```terraform
resource "aws_instance" "app_server" {
  ...
  provisioner "local-exec" {
    command = <<EOF
aws ec2 wait instance-status-ok --region ${var.region} --instance-ids ${self.id} \
&& ansible-playbook ../playbook-test.yml
EOF
  }
}
```

This does not work either because inmediatelly after instance creation, the config
file `ansible.cfg` is yet to be created.

Now, pushing the workaround a bit further:

```terraform
resource "aws_instance" "app_server" {
  ...
  provisioner "local-exec" {
    command = <<EOF
aws ec2 wait instance-status-ok --region ${var.region} --instance-ids ${self.id} \
&& while [ ! -f ansible.cfg ]; do sleep 1; done \
&& ansible-playbook playbook-test.yml
EOF
  }
}
```

**Now it never ends**.

The problem is that the resource never finishes, because of the while loop, so
there is no outputs to create the file from templates, then the while never
finishes.

There is a *sketchy* way to do this using
[Provisioners Without a Resource](https://www.terraform.io/docs/language/resources/provisioners/null_resource.html):

```terraform
resource "null_resource" "app_server" {
  provisioner "local-exec" {
    command = "ansible-playbook playbook-test.yml"
  }
  depends_on = [
    aws_instance.app_server,
  ]
}
```

## Ssh access

In order to connect to the machine:

```bash
ssh -i <key_file> <ip>
```

To obtain such command in `outputs.tf`:

```terraform
output "command_for_ssh" {
  value       = "ssh -i ${var.ssh_key_file} ubuntu@${aws_instance.app_server.public_ip}"
}
```

So for example:

```bash
...
Outputs:

command_for_ssh = "ssh -i ~/.ssh/juanpablo.pem ubuntu@34.208.15.205"
instance_id = "i-01a87ed23b3abc7ac"
instance_public_dns = "ec2-34-208-15-205.us-west-2.compute.amazonaws.com"
instance_public_ip = "34.208.15.205"

$ ssh -i ~/.ssh/juanpablo.pem ubuntu@34.208.15.205
Welcome to Ubuntu 16.04.7 LTS (GNU/Linux 4.4.0-1112-aws x86_64)

...

ubuntu@ip-172-31-27-95:~$ ls
carpeta-test
```

## Destroy

To put down all the infraestructure managed:

```bash
terraform destroy
```
