output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.app_server.id
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.app_server.public_ip
}

output "instance_public_dns" {
  description = "Public DNS of the EC2 instance"
  value       = aws_instance.app_server.public_dns
}

output "command_for_ssh" {
  value       = "ssh -i ${var.ssh_key_file} ubuntu@${aws_instance.app_server.public_ip}"

}

resource "local_file" "ansible_hosts" {
  content  = templatefile("${path.module}/templates/hosts.tpl", {
     vm_public_dns = aws_instance.app_server.public_dns,
     vm_key_file   = var.ssh_key_file
  })
  filename = "ansible/hosts"
}

resource "local_file" "ansible_cfg" {
  content  = templatefile("${path.module}/templates/ansible.cfg.tpl", {
     vm_user_name   = var.instance_user_name,
     vm_public_dns  = aws_instance.app_server.public_dns,
     vm_key_file    = var.ssh_key_file
  })
  filename = "ansible.cfg"
}