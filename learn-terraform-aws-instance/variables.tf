variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "ExampleAppServerInstance"
}

variable "instance_user_name" {
  description = "Value of the default user for the EC2 instance"
  type        = string
  default     = "ubuntu"
}

variable ssh_key_name {
  description = "Name of the pem key created at AWS console"
  type        = string
  default     = "juanpablo"
}

variable ssh_key_file {
  description = "Location of pem file"
  type        = string
  default     = "~/.ssh/juanpablo.pem"
}