terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-west-2"
}

resource "aws_instance" "app_server" {
  ami           = "ami-08d70e59c07c61a3a"
  instance_type = "t2.micro"
  key_name = var.ssh_key_name

  tags = {
    Name = var.instance_name
  }
}

resource "null_resource" "app_server" {
  provisioner "local-exec" {
    command = "ansible-playbook playbook-test.yml"
  }
  depends_on = [
    aws_instance.app_server,
  ]
}