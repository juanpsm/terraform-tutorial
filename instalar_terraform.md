# Terraform

## tfswitch

Para manejar versiones de terraform

```sh
$ sudo curl -L https://raw.githubusercontent.com/warrensbox/terraform-switcher/release/install.sh | bash
$ sudo tfswitch
2021/07/02 09:50:09 Creating directory for terraform: /root/.terraform.versions/
✔ 1.0.1
Downloading https://releases.hashicorp.com/terraform/1.0.1/terraform_1.0.1_linux_amd64.zip to terraform_1.0.1_linux_amd64.zip
Downloading ...
33044548 bytes downloaded.
Switched terraform to version "1.0.1" 
```

Pero no "veo" la instalación de terraform desde mi usuario.
Si instalo terraform por separado me instala la ultima versión y no hace caso
del cambio de version de tfswitch.

```sh
$ sudo apt-get remove terraform
$ sudo rm /usr/local/bin/tfswitch
$ sudo snap install tfswitch
$  tfswitch 
✔ 1.0.1
Error - Binary path does not exist: /usr/local/bin
Create binary path: /usr/local/bin for terraform installation
```

Viendo este [Troubleshoot](https://tfswitch.warrensbox.com/Troubleshoot/)

```sh
$ sudo snap remove tfswitch
$ wget https://raw.githubusercontent.com/warrensbox/terraform-switcher/release/install.sh  #Get the installer on to your machine
$ chmod 755 install.sh #Make installer executable
$ ./install.sh -b $HOME/.bin #Install tfswitch in a location you have permission
$ $HOME/.bin/tfswitch #test
✔ 1.0.0 *recent
2021/07/02 11:08:14 
		Unable to create new symlink.
		Maybe symlink already exist. Try removing existing symlink manually.
		Try running "unlink /usr/local/bin/terraform" to remove existing symlink.
		If error persist, you may not have the permission to create a symlink at /usr/local/bin/terraform.
		Error: symlink /home/lilith/.terraform.versions/terraform_1.0.0 /usr/local/bin/terraform: permission denied
```

Primero hay que agregar esa ruta al `PATH` así puedo correr 
directamente `tfswitch` y no `$HOME/.bin/tfswitch`.

Además hay que indicarle donde está el binario.
 
```sh
$ export PATH=$PATH:$HOME/.bin/
$ tfswitch -b $HOME/.bin/terraform
✔ 1.0.1 *recent
Switched terraform to version "1.0.1" 

$ terraform -version
Terraform v1.0.1
on linux_amd64
```

Success. Agrego a `.bash_profile` para que persista el export.

```bash
echo "export PATH=$PATH:$HOME/.bin" > .bash_profile
```

